#include "pch.h"
#include "enemy.h"



Enemy::Enemy()
{
	hp = 25; //hit points
	damage = 8;
	name = "enemy";
	isBoss = false;
	ability = "nothing";
}



//accessors
bool Enemy::getIsBoss(void) //get enemy boss
{
	return isBoss;
}

string Enemy::getAbility(void) //get enemy ability
{
	return ability;
}


//mutators
void Enemy::setIsBoss(bool ifBoss) //setting enemy to a boss
{
	isBoss = ifBoss;
}

void Enemy::setAbility(string newAbility) //setting enemy boss ability
{
	ability = newAbility;
}

//function
int Enemy::attackPl(Entity p)
{
	int tmpHP = p.getHp() - damage;
	p.setHp(tmpHP);
	return tmpHP; //damages player from the enemies damage
}

int Enemy::bossHpBoost()
{
	hp += 25;
	return hp;
}

int Enemy::bossDamageBoost()
{
	damage += 4;
	return damage;
}

bool Enemy::isEnemydead() //checks if enemies dead
{
	if (hp <= 0)
	{
		cout << "it's dead!" << endl; cout << "Enemy Hp: "; //if enemy has no health then this is printed
		return hp;
		return true;
	}
	else if (hp <= 10)
	{
		cout << "it's nearly dead!" << endl; cout << "Enemy Hp: "; //if enemy has less than 10 health then this is printed
		return hp;
		return false;
	}
	else if (hp <= 25)
	{
		cout << "keep going!" << endl; cout << "Enemy Hp: "; //if enemy has less than 25 health then this is printed
		return hp;
		return false;
	}
	else if (hp <= 40)
	{
		cout << "HIT AGAIN!" << endl; cout << "Enemy Hp: "; //if enemy has less than 40 health then this is printed
		return hp;
		return false;
	}
	return false;
}