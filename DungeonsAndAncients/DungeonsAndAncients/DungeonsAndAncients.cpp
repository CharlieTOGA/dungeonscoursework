#include "pch.h"
#include <iostream>
#include "dungeon.h" // dungeon header file
#include "entity.h" // entity header file
#include "enemy.h" // enemy header file
#include "player.h" // player header file
#include "weapon.h" //weapon header file
#include "Game.h"
#include <string> //includes strings
#include <stdlib.h> // rand() srand()
#include <time.h> // time(NULL)
#include <stack> //STL stack
using namespace std;
int main()
{
	//variables 
	Dungeon dungeonVar; //dungeon variable
	Weapon knightWeapon; //wooden sword variable
	Game * gameVar = new Game;
	stack<Enemy *> enemyStack; //stack of enemy pointers


	//dungeon type array
	string dungeonType[3];
	dungeonType[0] = "knight dungeon";
	dungeonType[1] = "viking dungeon"; // initialising the array
	dungeonType[2] = "samurai dungeon";

	for (int i = 0; i < 3; i++) //for loop creating 3 enemies on the stack
	{
		enemyStack.push(new Enemy); //for loop pushing 3 enemies to a stack
	}
	Enemy * undeadKnight = enemyStack.top(); //assigning a pointer to the top of the stack

	//rand and srand used randomise dungeon choice from dungeon array
	srand((unsigned int)time(NULL));
	int r2 = (rand() % 20) + 1;

	//start of game 
	gameVar->introduction();

cout << "please type a, b, or c." << endl << endl;
	char answer; // dungeon choice variable
	cin >> answer;
	cin.clear();
	cin.ignore(80, '\n'); //stack overflow comment gave me this solution

	//game loop for knight dungeon
	while (answer == 'a')
	{


		dungeonVar.setD(dungeonType[0]);
		cout << "you have chosen to explore " << dungeonVar.getD() << " wielding a wooden sword." << endl << endl; //dungeon path

cout << "enemy approaching, attack!" << endl << endl;

		gameVar->fightFunc(knightWeapon, undeadKnight); //fight between player and an undead knight
		delete undeadKnight; //undead knight dies and its memory address is emptied
		enemyStack.pop(); //enemy at the top of the stack is emptied

	
		cout << "Health Boost: " << gameVar->playerVar.healthBoost() << endl << endl;
		
		//enemy dropped new weapon
cout << "Wow, that knight dropped a stone sword! That's got to be more useful than the wooden one I've got." << endl << "Will I swap your sword for the stone one? y/n" << endl << endl;
		char wpnSwitch;
		cin >> wpnSwitch;
		cin.clear();
		cin.ignore(80, '\n');
		switch (wpnSwitch) //choice for the player to pick up weapon
		{
		case 'y':
			knightWeapon.setDamageL(8); knightWeapon.setDamageH(17);
			cout << "Very wise decision..." << endl << endl;
			break;
		case 'n':
			cout << "Sticking to the old trusty eh? lets hope it doesn't break." << endl << endl;
			break;
		default:
			cout << "The sword vanished in to thin air, returning to the underworld no doubt." << endl << endl;
			break;
		}


		//direction choice function
		gameVar->directionChoice();


cout << "Uh-oh... looks like another knight! That sword would be useful. Attack!!!" << endl;
		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //second fight
		delete undeadKnight;
		enemyStack.pop();

		//stamina boost
		cout << "Stamina boost: " << gameVar->playerVar.staminaBoost() << endl << endl;

cout << "Wow, that knight dropped an iron sword! That's got to be more useful than the one I've got." << "YOINK!" << endl << endl;
		knightWeapon.setDamageL(10); knightWeapon.setDamageH(20); //setting weapon damage higher for the iron sword

cout << "So... so tired. Been walking through this dungeon for hours now and no treasure." << endl << endl << endl << endl;
cout << "Wait whats that? treasu......      " << "L O U D  C R A S H!" << endl << endl << "That is one big Knight" << endl << "With this iron sword I will defeat you!!! ATTACK!" << endl;

//increasing boss damage
int tmbDamageBoss;
tmbDamageBoss = enemyStack.top()->bossDamageBoost();
enemyStack.top()->setDamage(tmbDamageBoss);

//increasing boss health
int tmbHpBoss;
tmbHpBoss = enemyStack.top()->bossHpBoost(); //adding boss values to the enemy
enemyStack.top()->setHp(tmbHpBoss);


		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //final boss fight
		delete undeadKnight;
		enemyStack.pop();
		undeadKnight = NULL; //avoiding memory leak
		
		gameVar->endOfStoryText(); //added exit(0); so the while quit while loop is irrelivant
		break;
	};


	
	
	//game loop for viking dungeon
	while (answer == 'b')
	{
		dungeonVar.setD(dungeonType[1]);
		cout << "you have chosen to expore " << dungeonVar.getD() << " wielding a wooden battle-Axe." << endl;

		cout << "enemy approaching, attack!" << endl << endl;

		gameVar->fightFunc(knightWeapon, undeadKnight); //fight between player and an undead knight
		delete undeadKnight; //undead knight dies and its memory address is emptied
		enemyStack.pop(); //enemy at the top of the stack is emptied


		cout << "Health Boost: " << gameVar->playerVar.healthBoost() << endl << endl;

		//enemy dropped new weapon
		cout << "Wow, that knight dropped a stone sword! That's got to be more useful than the wooden one I've got." << endl << "Will I swap your sword for the stone one? y/n" << endl << endl;
		char wpnSwitch;
		cin >> wpnSwitch;
		cin.clear();
		cin.ignore(80, '\n');
		switch (wpnSwitch) //choice for the player to pick up weapon
		{
		case 'y':
			knightWeapon.setDamageL(8); knightWeapon.setDamageH(17);
			cout << "Very wise decision..." << endl << endl;
			break;
		case 'n':
			cout << "Sticking to the old trusty eh? lets hope it doesn't break." << endl << endl;
			break;
		default:
			cout << "The sword vanished in to thin air, returning to the underworld no doubt." << endl << endl;
			break;

			delete gameVar;
			gameVar = NULL;
		}


		//left (dead) or right (enemy)
		gameVar->directionChoice();


		cout << "Uh-oh... looks like another Viking! That sword would be useful. Attack!!!" << endl;
		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //second fight
		delete undeadKnight;
		enemyStack.pop();

		//stamina boost
		cout << "Stamina boost: " << gameVar->playerVar.staminaBoost() << endl << endl;

		cout << "Wow, that knight dropped an iron battle-Axe! That's got to be more useful than the one I've got." << "YOINK!" << endl << endl;
		knightWeapon.setDamageL(10); knightWeapon.setDamageH(20); //setting weapon damage higher for the iron sword

		cout << "So... so tired. Been walking through this dungeon for hours now and no treasure." << endl << endl << endl << endl;
		cout << "Wait whats that? treasu......      " << "L O U D  C R A S H!" << endl << endl << "That is one big Knight" << endl << "With this iron sword I will defeat you!!! ATTACK!" << endl;

		//increasing boss damage
		int tmbDamageBoss;
		tmbDamageBoss = enemyStack.top()->bossDamageBoost();
		enemyStack.top()->setDamage(tmbDamageBoss);

		//increasing boss health
		int tmbHpBoss;
		tmbHpBoss = enemyStack.top()->bossHpBoost(); //adding boss values to the enemy
		enemyStack.top()->setHp(tmbHpBoss);


		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //final boss fight
		delete undeadKnight;
		enemyStack.pop();
		undeadKnight = NULL; //avoiding memory leak

		gameVar->endOfStoryText(); //added exit(0); so the while quit while loop is irrelivant
		break;

		delete gameVar;
		gameVar = NULL;
	};




	//game loop for samurai dungeon
	while (answer == 'c')
	{
		dungeonVar.setD(dungeonType[2]);
		cout << "you have chosen to explore " << dungeonVar.getD() << " wielding a wooden katana." << endl;

		cout << "enemy approaching, attack!" << endl << endl;

		gameVar->fightFunc(knightWeapon, undeadKnight); //fight between player and an undead knight
		delete undeadKnight; //undead knight dies and its memory address is emptied
		enemyStack.pop(); //enemy at the top of the stack is emptied


		cout << "Health Boost: " << gameVar->playerVar.healthBoost() << endl << endl;

		//enemy dropped new weapon
		cout << "Wow, that knight dropped a stone Katana! That's got to be more useful than the wooden one I've got." << endl << "Will I swap your sword for the stone one? y/n" << endl << endl;
		char wpnSwitch;
		cin >> wpnSwitch;
		cin.clear();
		cin.ignore(80, '\n');
		switch (wpnSwitch) //choice for the player to pick up weapon
		{
		case 'y':
			knightWeapon.setDamageL(8); knightWeapon.setDamageH(17);
			cout << "Very wise decision..." << endl << endl;
			break;
		case 'n':
			cout << "Sticking to the old trusty eh? lets hope it doesn't break." << endl << endl;
			break;
		default:
			cout << "The Katana vanished in to thin air, returning to the underworld no doubt." << endl << endl;
			break;
		}


		//left (dead) or right (enemy)
		gameVar->directionChoice();


		cout << "Uh-oh... looks like another Samurai! That Katana would be useful. Attack!!!" << endl;
		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //second fight
		delete undeadKnight;
		enemyStack.pop();

		//stamina boost
		cout << "Stamina boost: " << gameVar->playerVar.staminaBoost() << endl << endl;

		cout << "Wow, that knight dropped an iron Katana! That's got to be more useful than the one I've got." << "YOINK!" << endl << endl;
		knightWeapon.setDamageL(10); knightWeapon.setDamageH(20); //setting weapon damage higher for the iron sword

		cout << "So... so tired. Been walking through this dungeon for hours now and no treasure." << endl << endl << endl << endl;
		cout << "Wait whats that? treasu......      " << "L O U D  C R A S H!" << endl << endl << "That is one big Knight" << endl << "With this iron Katana I will defeat you!!! ATTACK!" << endl;

		//increasing boss damage
		int tmbDamageBoss;
		tmbDamageBoss = enemyStack.top()->bossDamageBoost();
		enemyStack.top()->setDamage(tmbDamageBoss);

		//increasing boss health
		int tmbHpBoss;
		tmbHpBoss = enemyStack.top()->bossHpBoost(); //adding boss values to the enemy
		enemyStack.top()->setHp(tmbHpBoss);


		undeadKnight = enemyStack.top();
		gameVar->fightFunc(knightWeapon, undeadKnight); //final boss fight
		delete undeadKnight;
		enemyStack.pop();
		undeadKnight = NULL; //avoiding memory leak

		gameVar->endOfStoryText(); //added exit(0); so the while quit while loop is irrelivant
	
		break;

		delete gameVar;
		gameVar = NULL;
	};
}