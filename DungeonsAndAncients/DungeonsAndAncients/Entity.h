#pragma once
#include <iostream>
#include <string>
using namespace std;




// entity class
class Entity
{
protected:
	int hp;
	int damage;
	string name; //entity variables

public:
	//entity constructor 
	Entity();

	//accessors
	int getHp(void);
	int getDamage(void);
	string getName(void);

	//mutators
	void setHp(int newHp);
	void setDamage(int newDamage);
	void setName(string newName);
};