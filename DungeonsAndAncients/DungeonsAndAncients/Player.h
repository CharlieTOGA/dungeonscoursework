#pragma once
#include <iostream>
#include <string>
#include "enemy.h"
#include "Entity.h"
using namespace std;

// player class deriving from parent class entity
class Player : public Entity
{
private:
	int stamina; //staminaVar

public:
	//player constructor
	Player();

	//accessors
	int getStamina();

	//mutators
	void setStaminaL(int updStaminaL);
	void setStaminaH(int updStaminaH);
	void setStaminaR(int updStaminaR);
	void setStamina(int newStamina);

	//functions
	int staminaReductionL();
	int staminaReductionH();
	int staminaReductionR();
	int healthBoost();
	int staminaBoost();
};