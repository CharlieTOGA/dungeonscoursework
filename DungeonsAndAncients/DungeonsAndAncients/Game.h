#pragma once
#include <iostream>
#include <string>
using namespace std;

#include "Enemy.h"
#include "Player.h"
#include "Weapon.h"
#include <stack>

class Game
{
public:
	Player playerVar; //variable for the player in the games

	
	Game(); //constructor

	//functions
	int fightFunc(Weapon wpn, Enemy * en);
	void introduction(void);
	void directionChoice(void);
	void endOfStoryText(void);
};