#pragma once
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include "enemy.h"
#include "player.h"
using namespace std;

#include <stdlib.h> // rand() srand()
#include <time.h> // time(NULL)

//weapon class
class Weapon
{
protected:
	int damageLight; //weapon variables
	int damageHeavy;
	int damageRandom;

public:
	Weapon();

	//accessor
	int getDamageL(void);
	int getDamageH(void);
	int getDamageR(void);

	//mutators
	void setDamageL(int newDamageL);
	void setDamageH(int newDamageH);
	void setDamageR(int newDamageR);

	//functions
	int attackL(Enemy en);
	int attackH(Enemy en);
	int attackR(Enemy en);
};