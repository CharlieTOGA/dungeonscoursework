#include "pch.h"
#include "entity.h"

Entity::Entity()
{
	hp = -1;
	damage = -3;
	name = "unnamed";
}

int Entity::getHp(void) //returns hp
{
	return hp;
}

int Entity::getDamage(void) //returns gamage
{
	return damage;
}

string Entity::getName(void) //returns name
{
	return name;
}

void Entity::setHp(int newHp) //returns hp
{
	hp = newHp;
}

void Entity::setDamage(int newDamage) //sets damage
{
	damage = newDamage;
}

void Entity::setName(string newName) //sets name
{
	name = newName;
}