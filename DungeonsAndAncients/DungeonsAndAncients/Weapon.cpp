#include "pch.h"
#include "weapon.h"

//constuctor
Weapon::Weapon()
{
	damageLight = 5;
	damageHeavy = 10;
	damageRandom = 15;
}

//accessors
int Weapon::getDamageL(void)
{
	return damageLight; ///returns light damage
}

int Weapon::getDamageH(void)
{
	return damageHeavy; //returns heavy damage
}

int Weapon::getDamageR(void)
{
	return damageRandom; //returns random damage

}

//mutators
void Weapon::setDamageL(int newDamageL)
{
	damageLight = newDamageL; //sets the light damage
}

void Weapon::setDamageH(int newDamageH)
{
	damageHeavy = newDamageH; //sets the heavy damage
}

void Weapon::setDamageR(int newDamageR)
{
	damageRandom = newDamageR; //sets the random damage
}


//functions
int Weapon::attackL(Enemy en)
{
	return en.getHp() - damageLight;
}

int Weapon::attackH(Enemy en)
{
	return en.getHp() - damageHeavy;
}

int Weapon::attackR(Enemy en)
{
	srand((unsigned int)time(NULL));
	int r2 = (rand() % 50) + 1;

	return en.getHp() - r2;
}