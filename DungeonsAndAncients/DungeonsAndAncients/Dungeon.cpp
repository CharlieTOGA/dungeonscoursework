#include "pch.h"
#include "dungeon.h"

Dungeon::Dungeon()
{
	dungeonType = "no dungeon";
}

void Dungeon::setD(string newDungeon)
{
	dungeonType = newDungeon;
}

string Dungeon::getD(void)
{
	return dungeonType;
}