#pragma once
#include <iostream>
#include <string>
using namespace std;

// dungeon class
class Dungeon
{
protected:
	string dungeonType; // dungeon array

public:
	//dungeon constructor
	Dungeon();


	//setter for dungeon type
	void setD(string newDungeon);

	//getter for dungeon type
	string getD(void);
};