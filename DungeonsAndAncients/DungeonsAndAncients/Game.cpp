#include "pch.h"
#include "Game.h"

Game::Game()
{
	Player playerVar;  //player in the game
}


int Game::fightFunc(Weapon wpn, Enemy * en) //fight between player and encountered enemy
{
	char attackChoice;

	while ((en->getHp() >= 1 ))
	{
		cin >> attackChoice;
		switch (attackChoice) //switch case for range of attacks
		{
		case '1':
			if (playerVar.getStamina() >= 5) 
			{
				en->setHp(wpn.attackL(*en)); //calls light attack
				playerVar.staminaReductionL();
				
			}
				else
				{
					cout << "You've don't have enough stamina for a light attack!" << endl;
				}
			break;
		case '2':
			if (playerVar.getStamina() >= 10)
			{
				en->setHp(wpn.attackH(*en)); //calls heavy attack
				playerVar.staminaReductionH();
				
			}
				else
				{
				cout << "You've don't have enough stamina for a heavy attack!" << endl;
				}
			break;

		case '3':
			if (playerVar.getStamina() >= 15)
			{
				en->setHp(wpn.attackR(*en)); //calls random attack
				playerVar.staminaReductionR();
				
			}
				else
				{
				cout << "You've don't have enough stamina for a random attack!" << endl;
				}
			break;

		default:
			cout << "you missed!" << endl; //player misses
			(playerVar.getStamina() - 2);
			break;
		}
		
		cin.clear();
		cin.ignore(80, '\n');
	
		en->isEnemydead(); //calls function to check if enemy is dead
		cout << en->getHp() << endl << endl;
		if (en->getHp() >= 1)
		{
			cout << "enemy hitting!" << endl << "Stamina: " << playerVar.getStamina() << endl;
			playerVar.setHp(en->attackPl(playerVar)); //enemy attacking player if it's not dead
			cout << "PlayerHp: " << playerVar.getHp() << endl; //printing players health
			
			if (playerVar.getHp() <= 0 || playerVar.getStamina() <= 2) //player dying
			{
				cout << "YOU WERE EXECUTED! GAME OVER" << endl << endl;
				exit(0); //exits game
			}
		}

	}
	return false;
}

void Game::introduction(void)
{
	cout << "Welcome to dungeons and Ancients, the text based linear dungeon crawl. Your objective is simple..." << endl << endl;
	cout << "Find the treasure and survive. No knowing whats down there..." << endl << endl;
	cout << "control scheme for attacking: " << endl << endl << "Light attack: '1' - deals 5 damage and costs 3 stamina." << endl << endl; //story stuff
	cout << "Heavy attack: '2' - deals 10 damage and costs 6 stamina." << endl << endl;
	cout << "Random attack: '3' - deals 0-50 damage and costs 12 stamina." << endl << endl;
	cout << "Be careful of your stamina... if you go lower than 3 you may die due to lack of strength to carry on fighting" << endl << endl;
	cout << "Good luck. Time to take your pick..." << endl << endl;
}

void Game::directionChoice(void) //direction function
{
	//left (dead) or right (enemy)
	cout << "A split in the dungeon lies ahead... Which way will you go?(l/r)" << endl << endl;
	char direction;
	cin >> direction;
	switch (direction)
	{
	case 'l':
		cout << "This looks promising..." << endl << endl;
		break;
	case 'r':
		cout << "I stepped on a booby trap and were crushed by rocks, GAME OVER" << endl << endl;
		exit(0); //end game
		break;
	default:
		cout << "My indecisiveness lead the cave to dungeon to cave in on itself, GAME OVER" << endl << endl;
		exit(0); //end game
		break;
	}
}

void Game::endOfStoryText(void)
{
	cout << "Thats got to be it surely... YEEEEEES treasure!!!! I'M RICHHHHHH" << endl << endl;
	cout << "Congratulations! You have completed Dungeons & Ancients, thankyou for playing." << endl << endl;
	exit(0); //stack overflow comment gave me this solution
}
