#pragma once
#include <iostream>
#include <string>
using namespace std;

#include "Entity.h"
#include "Player.h"


//enemy class deriving from entity class
class Enemy : public Entity
{
private:
	bool isBoss; //boss option to enemy
	string ability; //special abilities, mainly for bosses


public:
	//enemy constructor
	Enemy();


	//accessors
	bool getIsBoss(void);
	string getAbility(void);

	//mutators
	void setIsBoss(bool ifBoss);
	void setAbility(string newAbility);

	//function prototypes
	bool isEnemydead();
	int attackPl(Entity p);
	
	int  bossHpBoost();
	int bossDamageBoost();
};