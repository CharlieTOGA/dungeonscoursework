#include "pch.h"
#include "player.h"

//player constructor
Player::Player()
{
	hp = 100;
	damage = 0;
	name = "player";
	stamina = 50;
}

int Player::getStamina() //gets the players stamina
{
	return stamina;
}

void Player::setStaminaL(int updStaminaL) //sets stamina after different attacks, applies for the other sets
{
	stamina = updStaminaL;
}

void Player::setStaminaH(int updStaminaH)
{
	stamina = updStaminaH;
}

void Player::setStaminaR(int updStaminaR)
{
	stamina = updStaminaR;
}

void Player::setStamina(int newStamina)
{
	stamina = newStamina;
}

int Player::staminaReductionL()
{
	stamina -= 3;
	return stamina;
}

int Player::staminaReductionH()
{
	stamina -= 6;
	return stamina;
}

int Player::staminaReductionR()
{
	stamina -= 12;
	return stamina;
}

int Player::healthBoost() //boosts players health after fight
{
	
	hp += 20;
	return hp;
}

int Player::staminaBoost()
{
	int tmpStam;
	tmpStam = stamina += 30;
	return stamina;
}
